# TEAM Framework Essentials
This is a meta-package which is required by TEAM Framework >=9.0 to pull all
dependencies used by the framework's core components.

By itself this package doesn't do anything but define the authors and set required
dependencies.

## Install with Composer
To install with [Composer](https://getcomposer.org/), simply require the latest version of this package.
```
composer require team/framework-essentials
```